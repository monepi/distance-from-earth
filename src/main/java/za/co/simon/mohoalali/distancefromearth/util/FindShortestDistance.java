package za.co.simon.mohoalali.distancefromearth.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.simon.mohoalali.distancefromearth.model.Edge;
import za.co.simon.mohoalali.distancefromearth.model.Graph;
import za.co.simon.mohoalali.distancefromearth.model.Node;

import java.util.HashMap;

public class FindShortestDistance {

    private static Logger logger = LoggerFactory.getLogger(FindShortestDistance.class);

    public static HashMap<Node, Double> FindShortestDistanceFromEarth(Node source, Node destination, Graph graph) {

        HashMap<Node, Node> changedAt = new HashMap<>();
        changedAt.put(source, null);

        HashMap<Node, Double> shortestPathMap = new HashMap<>();


        setAllNodesShortestPathToInfinityExceptStartNode(source, graph, shortestPathMap);

        for (Edge edge : source.getEdges()) {
            shortestPathMap.put(edge.getDestination(), edge.getWeight());
            changedAt.put(edge.getDestination(), source);
        }

        source.visit();

        while (true) {
            Node currentNode = closestReachableUnvisited(shortestPathMap, graph);
            if (currentNode == null) {
                logger.info("There isn't a path between {} and {}", source.getPlanetName(), destination.getPlanetName());
                return shortestPathMap;
            }

            if (getShortestPathToDestination(source, destination, changedAt, shortestPathMap, currentNode)) {
                return shortestPathMap;
            }
            currentNode.visit();

            checkIfEdgeHasBeenVisited(changedAt, shortestPathMap, currentNode);
        }
    }

    private static void setAllNodesShortestPathToInfinityExceptStartNode(Node source, Graph graph, HashMap<Node, Double> shortestPathMap) {
        for (Node node : graph.getNodes()) {
            if (node == source) {
                shortestPathMap.put(source, 0.0);
            }
            else {
                shortestPathMap.put(node, Double.POSITIVE_INFINITY);
            }
        }
    }

    private static void checkIfEdgeHasBeenVisited(HashMap<Node, Node> changedAt, HashMap<Node, Double> shortestPathMap, Node currentNode) {
        for (Edge edge : currentNode.getEdges()) {
            if (edge.getDestination().isVisited())
                continue;

            if (shortestPathMap.get(currentNode)
                    + edge.getWeight()
                    < shortestPathMap.get(edge.getDestination())) {
                shortestPathMap.put(edge.getDestination(),
                        shortestPathMap.get(currentNode) + edge.getWeight());
                changedAt.put(edge.getDestination(), currentNode);
            }
        }
    }

    private static boolean getShortestPathToDestination(Node source, Node destination, HashMap<Node, Node> changedAt, HashMap<Node, Double> shortestPathMap, Node currentNode) {
        if (currentNode == destination) {
            logger.info("The path with the smallest weight between {} and {} is : ",
                     source.getPlanetName(),  destination.getPlanetName() );

            Node child = destination;
            StringBuilder path = new StringBuilder(destination.getPlanetName());
            while (true) {
                Node parent = changedAt.get(child);
                if (parent == null) {
                    break;
                }
                path.insert(0, parent.getPlanetName() + " ");
                child = parent;
            }
            logger.info(path.toString());
            logger.info("The path costs: {}", shortestPathMap.get(destination));
            return true;
        }
        return false;
    }

    private static Node closestReachableUnvisited(HashMap<Node, Double> shortestPathMap, Graph graph) {
        double shortestDistance = Double.POSITIVE_INFINITY;
        Node closestReachableNode = null;
        for (Node node : graph.getNodes()) {
            if (node.isVisited())
                continue;

            double currentDistance = shortestPathMap.get(node);
            if (currentDistance == Double.POSITIVE_INFINITY)
                continue;

            if (currentDistance < shortestDistance) {
                shortestDistance = currentDistance;
                closestReachableNode = node;
            }
        }
        return closestReachableNode;
    }

}
