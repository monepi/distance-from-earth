package za.co.simon.mohoalali.distancefromearth.model;

public class Edge implements Comparable<Edge> {

    private Node source;
    private Node destination;
    private Double weight;

    public Edge(Node source, Node destination, Double distance) {
        this.source = source;
        this.destination = destination;
        this.weight = distance;
    }

    public Node getSource() {
        return source;
    }

    public Node getDestination() {
        return destination;
    }

    public Double getWeight() {
        return weight;
    }

    public void setSource(Node source) {
        this.source = source;
    }

    public void setDestination(Node destination) {
        this.destination = destination;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Edge {" +
                "source=" + source +
                ", destination=" + destination +
                ", weight=" + weight +
                '}';
    }

    @Override
    public int compareTo(Edge edge) {
        if (this.weight > edge.weight) {
            return 1;
        }
        return -1;
    }
}
