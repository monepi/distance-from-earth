package za.co.simon.mohoalali.distancefromearth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DistanceFromEarthApplication {

	public static void main(String[] args) {

		SpringApplication.run(DistanceFromEarthApplication.class, args);
	}



}
