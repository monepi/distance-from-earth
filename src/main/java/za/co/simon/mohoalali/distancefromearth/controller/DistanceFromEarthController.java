package za.co.simon.mohoalali.distancefromearth.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import za.co.simon.mohoalali.distancefromearth.service.CalculateDistanceService;
import za.co.simon.mohoalali.distancefromearth.util.GetNodeLists;

@Controller
public class DistanceFromEarthController {

    private static Logger logger = LoggerFactory.getLogger(DistanceFromEarthController.class);

    private CalculateDistanceService calculateDistanceService;

    public DistanceFromEarthController(CalculateDistanceService calculateDistanceService) {
        this.calculateDistanceService = calculateDistanceService;
    }

    @GetMapping("/")
    public String showIndexForm(Model model) {
        model.addAttribute("planets", GetNodeLists.getNodes());
        return "index";
    }

    @PostMapping("/check-distance")
    public String checkDistanceBetweenSourceAndDestination(@RequestParam(value = "destination") String destination,
                                                           Model model) {
        model.addAttribute("planets", GetNodeLists.getNodes());
        try {
            model.addAttribute(
                    "distance", "Shortest distance between Earth and destination " + destination + " is : " + calculateDistanceService.execute(destination)
            );
        } catch (NullPointerException np) {
            model.addAttribute(
                    "distance", "There isn't a path between Earth and " + destination
            );
        }
        return "index";
    }

}
