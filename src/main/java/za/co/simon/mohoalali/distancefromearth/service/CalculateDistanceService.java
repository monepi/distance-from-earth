package za.co.simon.mohoalali.distancefromearth.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import za.co.simon.mohoalali.distancefromearth.model.Graph;
import za.co.simon.mohoalali.distancefromearth.model.Node;
import za.co.simon.mohoalali.distancefromearth.util.FindShortestDistance;
import za.co.simon.mohoalali.distancefromearth.util.GetNodeLists;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CalculateDistanceService {

    private static Logger logger = LoggerFactory.getLogger(CalculateDistanceService.class);

    public double execute(String destination) {
        Graph graph = new Graph(true);

        Node a = new Node("A", "Earth");
        Node b = new Node("B", "Moon");
        Node c = new Node("C", "Jupiter");
        Node d = new Node("D", "Venus");
        Node e = new Node("E", "Mars");
        Node f = new Node("F", "Saturn");
        Node g = new Node("G", "Uranus");
        Node h = new Node("H", "Pluto");
        Node i = new Node("I", "Neptune");
        Node j = new Node("J", "Mercury");
        Node k = new Node("K", "Alpha Centauri");
        Node l = new Node("L", "Luhman 16");
        Node m = new Node("M", "Epsilon Eridani");
        Node n = new Node("N", "Groombridge 34");
        Node o = new Node("O", "Epsilon Indi");
        Node p = new Node("P", "Tau Ceti");
        Node q = new Node("Q", "Kapteyn's star");
        Node r = new Node("R", "Gliese 687");
        Node s = new Node("S", "Gliese 674");
        Node t = new Node("T", "Gliese 876#");
        Node u = new Node("U", "Gliese 832");
        Node v = new Node("V", "Fomalhaut");
        Node w = new Node("W", "Virginis");
        Node x = new Node("X", "HD 102365");
        Node y = new Node("Y", "Gliese 176");
        Node z = new Node("Z", "Gliese 436");
        Node a_ = new Node("A'", "Pollux");
        Node b_ = new Node("B'", "Gliese 86");
        Node c_ = new Node("C'", "HIP 57050");
        Node d_ = new Node("D'", "Piscium");
        Node e_ = new Node("E'", "GJ 1214");
        Node f_ = new Node("F'", "Upsilon Andromedae");
        Node g_ = new Node("G'", "Gamma Cephei");
        Node h_ = new Node("H'", "HD 176051");
        Node i_ = new Node("I'", "Gliese 317");
        Node j_ = new Node("J'", "HD 38858");
        Node k_ = new Node("K'", "Ursae Majoris");


        graph.addEdge(a, b, 0.44);
        graph.addEdge(a, c, 1.89);
        graph.addEdge(a, c, 0.10);
        graph.addEdge(b, h, 2.44);
        graph.addEdge(b, e, 3.45);
        graph.addEdge(c, f, 0.45);
        graph.addEdge(d, l, 2.34);
        graph.addEdge(d, m, 2.61);
        graph.addEdge(h, g, 3.71);
        graph.addEdge(e, i, 8.09);
        graph.addEdge(f, j, 5.46);
        graph.addEdge(f, k, 3.67);
        graph.addEdge(g, z, 5.25);
        graph.addEdge(i, z, 13.97);
        graph.addEdge(i, j, 14.78);
        graph.addEdge(l, t, 15.23);
        graph.addEdge(t, n, 10.43);
        graph.addEdge(t, s, 14.22);
        graph.addEdge(s, o, 6.02);
        graph.addEdge(o, u, 5.26);
        graph.addEdge(j, r, 12.34);
        graph.addEdge(r, p, 10.10);
        graph.addEdge(x, k_, 19.94);
        graph.addEdge(p, u, 9.31);
        graph.addEdge(u, k_, 21.23);
        graph.addEdge(u, j_, 25.96);
        graph.addEdge(j_, v, 3.16);
        graph.addEdge(k_, v, 20.42);
        graph.addEdge(j_, i_, 17.10);
        graph.addEdge(y, a_, 19.52);
        graph.addEdge(a_, b_, 31.56);
        graph.addEdge(b_, c_, 23.13);
        graph.addEdge(k_, w, 28.89);
        graph.addEdge(w, c_, 10.64);
        graph.addEdge(w, e_, 36.19);
        graph.addEdge(c_, d_, 36.48);
        graph.addEdge(e_, e_, 41.26);
        graph.addEdge(e_, f_, 42.07);
        graph.addEdge(f_, g_, 17.63);
        graph.addEdge(g_, h_, 40.48);


        List<Node> destinationNode = GetNodeLists.getNodes()
                .stream()
                .filter(node -> node.getPlanetName().equalsIgnoreCase(destination)).collect(Collectors.toList());

        logger.error("Planet Node being checked : {}", destinationNode.get(0).getPlanetNode());

        switch (destinationNode.get(0).getPlanetNode()) {
            case "A":
                 return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, a, graph).get(a);
            case "B":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, b, graph).get(b);
            case "C":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, c, graph).get(c);
            case "D":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, d, graph).get(d);
            case "E":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, e, graph).get(e);
            case "F":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, f, graph).get(f);
            case "G":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, g, graph).get(g);
            case "H":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, h, graph).get(h);
            case "I":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, i, graph).get(i);
            case "J":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, j, graph).get(j);
            case "K":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, k, graph).get(k);
            case "L":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, l, graph).get(l);
            case "M":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, m, graph).get(m);
            case "N":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, n, graph).get(n);
            case "O":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, o, graph).get(o);
            case "P":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, p, graph).get(p);
            case "Q":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, q, graph).get(q);
            case "R":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, r, graph).get(r);
            case "S":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, s, graph).get(s);
            case "T":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, t, graph).get(t);
            case "U":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, u, graph).get(u);
            case "V":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, v, graph).get(v);
            case "W":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, w, graph).get(w);
            case "X":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, x, graph).get(x);
            case "Y":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, y, graph).get(y);
            case "Z":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, z, graph).get(z);
            case "A'":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, a_, graph).get(a_);
            case "B'":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, b_, graph).get(b_);
            case "C'":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, c_, graph).get(c_);
            case "D'":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, d_, graph).get(d_);
            case "E'":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, e_, graph).get(e_);
            case "F'":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, f_, graph).get(f_);
            case "G'":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, g_, graph).get(g_);
            case "H'":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, h_, graph).get(h_);
            case "I'":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, i_, graph).get(i_);
            case "J'":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, j_, graph).get(j_);
            case "K'":
                return FindShortestDistance
                        .FindShortestDistanceFromEarth(a, k_, graph).get(k_);
            default:
                throw new IllegalStateException("Unexpected value: " + destinationNode.get(0).getPlanetNode());
        }
    }

}
