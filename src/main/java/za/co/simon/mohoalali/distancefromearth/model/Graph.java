package za.co.simon.mohoalali.distancefromearth.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;


public class Graph {
    private Set<Node> nodes = new HashSet<>();
    private boolean directed;

    private static Logger logger = LoggerFactory.getLogger(Graph.class);

    public Graph(boolean directed) {
        this.directed = directed;
    }

    public Set<Node> getNodes() {
        return nodes;
    }

    public boolean isDirected() {
        return directed;
    }

    public void addNode(Node... node) {
        nodes.addAll(Arrays.asList(node));
    }

    public void addEdge(Node source, Node destination, double weight) {
        nodes.add(source);
        nodes.add(destination);

        addEdgeHelper(source, destination, weight);

        if (!directed && source != destination) {
            addEdgeHelper(destination, source, weight);
        }
    }

    private void addEdgeHelper(Node source, Node destination, double weight) {
        for (Edge edge : source.getEdges()) {
            if (edge.getSource() == source && edge.getDestination() == destination) {
                edge.setWeight(weight);
                return;
            }
        }
        source.getEdges().add(new Edge(source, destination, weight));
    }

    public boolean hasEdge(Node source, Node destination) {
        LinkedList<Edge> edges = source.getEdges();
        for (Edge edge : edges) {
            if (edge.getDestination() == destination) {
                return true;
            }
        }
        return false;
    }

    public void resetNodesVisited() {
        nodes.forEach(Node::unvisit);
    }

    public void logEdges() {
        for (Node node : nodes) {
            LinkedList<Edge> edges = node.getEdges();

            if (edges.isEmpty()) {
                logger.debug("Node {} has no edges.", node.getPlanetName());
            }
            logger.debug("Node {} has edges to below: ", node.getPlanetName());
            edges.forEach(Graph::printEdges);
        }
    }

    private static void printEdges(Edge edge) {
        logger.debug("Edge name : {}, Edge weight : {}", edge.getDestination().getPlanetName(), edge.getWeight());
    }
}
