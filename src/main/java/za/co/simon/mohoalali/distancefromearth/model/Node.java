package za.co.simon.mohoalali.distancefromearth.model;

import java.util.LinkedList;

public class Node {
    private String planetNode;
    private String planetName;
    private boolean visited = false;
    private LinkedList<Edge> edges = new LinkedList<>();

    public Node(String planetNode, String planetName) {
        this.planetNode = planetNode;
        this.planetName = planetName;
    }

    public boolean isVisited() {
        return visited;
    }

    public void visit() {
        visited = true;
    }

    public void unvisit() {
        visited = false;
    }

    public String getPlanetNode() {
        return planetNode;
    }

    public void setPlanetNode(String planetNode) {
        this.planetNode = planetNode;
    }

    public String getPlanetName() {
        return planetName;
    }

    public void setPlanetName(String planetName) {
        this.planetName = planetName;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public LinkedList<Edge> getEdges() {
        return edges;
    }

    public void setEdges(LinkedList<Edge> edges) {
        this.edges = edges;
    }
}
