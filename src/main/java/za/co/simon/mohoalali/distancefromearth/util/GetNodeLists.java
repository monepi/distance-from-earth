package za.co.simon.mohoalali.distancefromearth.util;

import za.co.simon.mohoalali.distancefromearth.model.Node;

import java.util.ArrayList;
import java.util.List;

public class GetNodeLists {

    public static List<Node> getNodes() {
        List<Node> nodes = new ArrayList<>();
        nodes.add(new Node("A", "Earth"));
        nodes.add(new Node("B", "Moon"));
        nodes.add(new Node("C", "Jupiter"));
        nodes.add(new Node("D", "Venus"));
        nodes.add(new Node("E", "Mars"));
        nodes.add(new Node("F", "Saturn"));
        nodes.add(new Node("G", "Uranus"));
        nodes.add(new Node("H", "Pluto"));
        nodes.add(new Node("I", "Neptune"));
        nodes.add(new Node("J", "Mercury"));
        nodes.add(new Node("K", "Alpha Centauri"));
        nodes.add(new Node("L", "Luhman 16"));
        nodes.add(new Node("M", "Epsilon Eridani"));
        nodes.add(new Node("N", "Groombridge 34"));
        nodes.add(new Node("O", "Epsilon Indi"));
        nodes.add(new Node("P", "Tau Ceti"));
        nodes.add(new Node("Q", "Kapteyn's star"));
        nodes.add(new Node("R", "Gliese 687"));
        nodes.add(new Node("S", "Gliese 674"));
        nodes.add(new Node("T", "Gliese 876#"));
        nodes.add(new Node("U", "Gliese 832"));
        nodes.add(new Node("V", "Fomalhaut"));
        nodes.add(new Node("W", "Virginis"));
        nodes.add(new Node("X", "HD 102365"));
        nodes.add(new Node("Y", "Gliese 176"));
        nodes.add(new Node("Z", "Gliese 436"));
        nodes.add(new Node("A'", "Pollux"));
        nodes.add(new Node("B'", "Gliese 86"));
        nodes.add(new Node("C'", "HIP 57050"));
        nodes.add(new Node("D'", "Piscium"));
        nodes.add(new Node("E'", "GJ 1214"));
        nodes.add(new Node("F'", "Upsilon Andromedae"));
        nodes.add(new Node("G'", "Gamma Cephei"));
        nodes.add(new Node("H'", "HD 176051"));
        nodes.add(new Node("I'", "Gliese 317"));
        nodes.add(new Node("J'", "HD 38858"));
        nodes.add(new Node("K'", "Ursae Majoris"));
        return nodes;
    }


}
